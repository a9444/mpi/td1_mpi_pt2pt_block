#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <mpi.h>

#define TAG_DATA    3000

int main(int argc, char **argv)
{
  int rank, nproc, nint;
  int *data = NULL;

  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);

  if (rank == 0)
  {
    MPI_Status sta;

    for(int islv = 0 ; islv < nproc-1 ; islv++)
    {
      /* On attend l'arrivee d'un message de n'importe qui
	 dont on ne connait pas la taille => MPI_Probe
	 */
      MPI_Probe(MPI_ANY_SOURCE, TAG_DATA, MPI_COMM_WORLD, &sta);

      /* Il faut recuperer la taille du message en nombre d'entiers => nint
	 Puis recevoir le message proprement dit => MPI_Recv
	 */
      MPI_Get_count(&sta, MPI_INT, &nint);

      /* Allocation du tableau avant de le recevoir
      */
      data = (int*)realloc(data, nint*sizeof(int));

      MPI_Recv(data, nint, MPI_INT, sta.MPI_SOURCE, sta.MPI_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

      /* Une fois le message reçu, on calcule la somme et on l'affiche
      */
      int sum=0;
      for(int i=0 ; i<nint ; i++) {
	sum += data[i];
      }
      printf("Rank 0 : message reçu de %d (size, sum) = (%d, %d)\n", sta.MPI_SOURCE, nint, sum);
    }
  }
  else
  {
    // On tire au hasard la taille du message
    srand((rank+1)*time(NULL));
    nint = (rand() % 100)+1;

    data = (int*)malloc(nint*sizeof(int));
    int sum=0;
    for(int i=0 ; i<nint ; i++) {
      data[i] = (rank+1);
      sum += data[i];
    }

    MPI_Send(data, nint, MPI_INT, 0, TAG_DATA, MPI_COMM_WORLD);
    printf("Rank %d : message envoyé à 0 (size, sum) = (%d, %d)\n", rank, nint, sum);
  }

  free(data);

  MPI_Finalize();

  return 0;
}

